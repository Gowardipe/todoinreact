import React from "react";
import FormCheck from "react-bootstrap/FormCheck";
import Card from "react-bootstrap/Card";
import moment from 'moment';
const TaskDetail = props => {
  const { id, taskDescription, isCompleted, deadline } = props.task;
  return (
    <Card
      text={isCompleted ? "success" : "danger"}
      className={"marginTop10 col-lg-12 alignLeft taskDetailFlex"}
      border={isCompleted ? "success" : "danger"}
    >
      <FormCheck>
        <FormCheck.Input
          onChange={event => props.onCheckChanged(id, event.target.checked)}
          type={"checkbox"}
          checked={isCompleted}
        />
      </FormCheck>
      <span className={isCompleted ? "markDone limitWord" : "limitWord"}>
        {taskDescription}
      </span>
      <span className={"paddingLeft20"}>{moment(deadline).format( 'DD-MM-YYYY')}</span>
    </Card>
  );
};

export default TaskDetail;
