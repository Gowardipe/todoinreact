import React, {Component} from "react";
import {Button, Modal, Nav, Tab} from "react-bootstrap";
import ToDoForm from "./ToDoForm";
import ToDoList from "./ToDoList";
import Switch from 'react-toggle-switch';


class ToDo extends Component {
  constructor(props) {
    super();
    console.log('props', props);
    this.state = {
      showForm: false,
      tasks: props.todos || [],
      viewCompletedTask: false,
      switched: false
    };
  }

  componentWillReceiveProps = (newProps, oldProps) => {
    console.log('new props ', newProps);
    if(newProps !== oldProps) {

      this.setState({tasks: newProps.todos});
    }
  }
  addTask = task => {
    this.props.addTask(task)
    .then(() => this.props.fetchTodos())
    .catch(console.error)
    this.toggleShowForm();
  };

  toggleShowForm = () => {
    this.setState({ showForm: !this.state.showForm });
  };
  
  
  handleSwitch = () => {
    this.props.toggleReminderForUser(!this.props.reminderStatus);
  }
  

  handleCheckChange = (id, isCompleted) => {
    const index = this.findTaskIndexById(this.state.tasks, id);
    const tasks = [...this.state.tasks];
    if (index !== -1) {
      tasks[index].isCompleted = isCompleted;
      this.setState({ tasks });
    }
  };

  findTaskIndexById = (tasks, id) => {
    let taskIndex = -1;
    tasks.forEach((task, index) => {
      if (task.id === id) taskIndex = index;
    });
    return taskIndex;
  };

  viewCompletedTasks = isCompleted => {
    this.setState({ viewCompletedTask: isCompleted });
  };

  filterTasks = () => {
    const { tasks, viewCompletedTask } = this.state;
    console.log(this.state);
    return tasks.filter(task => task.isCompleted === viewCompletedTask);
  };

  render() {
    return (
      <div className={'containerFlex'}>
        <h1>TO-DO</h1>
        <br />
        <Switch onClick={this.handleSwitch} on={this.props.reminderStatus}/>
 
        <Button id="addToDo" variant="outline-success" onClick={this.toggleShowForm}>
          Create Task
        </Button>
        {this.renderModal()}
        <Tab.Container id="left-tabs-example" defaultActiveKey="first">
          <Nav
            variant="pills"
            className="flex-row justify-content-center marginTop10"
          >
            <Nav.Item>
              <Nav.Link
                eventKey="first"
                onSelect={()=>this.viewCompletedTasks(false)}
              >
                Tasks pending
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link
                eventKey="second"
                onSelect={()=>this.viewCompletedTasks(true)}
              >
                Tasks Completed
              </Nav.Link>
            </Nav.Item>
          </Nav>
        </Tab.Container>
        <ToDoList
          tasks={this.filterTasks()}
          onCheckChanged={this.handleCheckChange}
        />
      </div>
    );
  }
  renderModal() {
    return (
      <Modal
        show={this.state.showForm}
        onHide={this.toggleShowForm}
        backdrop={"static"}
      >
        <Modal.Header closeButton>Add a TODO</Modal.Header>
        <Modal.Body>
          <ToDoForm addToDo={this.addTask} />
        </Modal.Body>
      </Modal>
    );
  }
}

export default ToDo;
