import React from "react";
import TaskDetail from "./taskDetail";

const ToDoList = props => {

  function renderTasks() {
    return (
      <div style={{width: 350}}>
        {props.tasks.map((task) => (
          <TaskDetail key={task._id} task={task} onCheckChanged={props.onCheckChanged}/>
        ))}
      </div>
    );
  }

  return <div>{renderTasks()}</div>;
};

export default ToDoList;
