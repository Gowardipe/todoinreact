export default class Task {
    constructor({instanceId, taskDescription,isCompleted, deadline}) {
        this.instanceId = instanceId;
        this.taskDescription = taskDescription || "";
        this.isCompleted = isCompleted || false;
        this.deadline = deadline || '';
        this.isNotified = false;
    }
}