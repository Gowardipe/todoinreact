import React from 'react';
import ReactDOM from 'react-dom';
import App from '../App';
import {shallow} from "enzyme";
import ToDo from "../ToDo";

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('should contain todo component', () => {
   let appComponent = shallow(<App/>);

    expect(appComponent.find(ToDo)).toHaveLength(1);
});
