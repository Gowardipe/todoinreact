import React from "react";
import { shallow } from "enzyme";
import ToDo from "../ToDo";
import ToDoForm from "../ToDoForm";
import {Modal} from "react-bootstrap";

describe('ToDO', () => {
    it('should render without crashing', () => {
        shallow(<ToDo/>);
    });

    it('should render add button for todos', () => {
        const toDoComponent = shallow(<ToDo/>);

        expect(toDoComponent.find("#addToDo")).toHaveLength(1);
        expect(toDoComponent.find("#addToDo").text()).toBe('Create Task');
    });

    it('should contain modal and ToDoForm', () => {
        const toDoComponent = shallow(<ToDo/>);

        expect(toDoComponent.find(Modal)).toHaveLength(1);
        expect(toDoComponent.find(ToDoForm)).toHaveLength(1);
    });

    it('should open form on click of create task', () => {
        const toDoComponent = shallow(<ToDo/>);
        const addToDobutton = toDoComponent.find('#addToDo');

        addToDobutton.simulate('click');

        const modal = toDoComponent.find(Modal);
        expect(modal.props().show).toBeTruthy();
    });
});