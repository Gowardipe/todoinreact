import ToDoForm from "../ToDoForm";
import React from "react";
import {shallow} from "enzyme";

describe('ToDo Form', () => {
    it('should render without crashing', () => {
        shallow(<ToDoForm/>);
    });
    it('should contain input to add todo', () => {
        const todoForm = shallow(<ToDoForm/>);

        expect(todoForm.find('#todoInput')).toHaveLength(1);
    });
});