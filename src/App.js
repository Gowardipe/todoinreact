import React from "react";
import "./App.css";
import ToDo from "./ToDo";
import {
  Stitch,
  AnonymousCredential,
  RemoteMongoClient
} from "mongodb-stitch-browser-sdk";

class App extends React.Component {
  state = {
    reminderStatus: false,
    todos: []
  }
  componentDidMount() {
    this.client = Stitch.initializeDefaultAppClient("todoapp-aophy");
    const mongodb = this.client.getServiceClient(
      RemoteMongoClient.factory,
      "mongodb-atlas"
    );
    this.db = mongodb.db("todo_db");
    this.login();
  }
  generateAndSendTokenToDb = () => {
    if(window) {
      let request = window.indexedDB.open('fcm_token_details_db');
      let indexedDb;
      request.onerror = function (event) {
        console.log('error: ', event);
      };
  
      request.onsuccess = (event) => {
        indexedDb = request.result; 
        let tx = indexedDb.transaction('fcm_token_object_Store', 'readonly');
        let store = tx.objectStore('fcm_token_object_Store');
        let itemsStore = store.getAll();
        itemsStore.onsuccess = (event) => this.saveTokenForInstance(itemsStore.result[0].fcmToken);
        itemsStore.onerror = (event) => console.log('store not found');
      };
    }
  }
  saveTokenForInstance = (fcmToken) => {
    this.setState({fcmToken}, () => {
      const {instanceId,reminderStatus, fcmToken} = this.state;
      this.db.collection('instance_tokens')
        .findOneAndUpdate({instanceId},{instanceId,reminderStatus, fcmToken})
        .then((result)=>{
          if(result === null){
            this.db.collection('instance_tokens')
            .insertOne({instanceId, fcmToken, reminderStatus})
            .then(()=>console.log('successfully added'))
            .catch(console.error)
          }else{
            const {instanceId, fcmToken, reminderStatus} = result;
            this.setState({instanceId, fcmToken, reminderStatus});
          }
        })
        .catch(console.error);
  });
  }
  render() {
    return (
      <div className="App">
        <ToDo toggleReminderForUser={this.toggleReminderForUser} reminderStatus={this.state.reminderStatus || false}
              fetchTodos={this.fetchTodos}
              addTask={this.addTask}
              todos={this.state.todos}/>
      </div>
    );
  }

  login = () => {
    this.client.auth
      .loginWithCredential(new AnonymousCredential())
      .then((res) => this.updateInstanceIdWithToken(res))
      .catch(console.error);
  };
  updateInstanceIdWithToken = ({ id }) => {
    this.setState({instanceId : id},()=>{
      // const {instanceId} = this.state;
      // this.db
      // .collection("instance_tokens")
      // .findOne({instanceId}).then((result) => {
      //   if(!!result) {
      //     const {instanceId, fcmToken, reminderStatus} = result;
      //     this.setState({instanceId, fcmToken, reminderStatus});
      //   }else{
          
      //   }
      // }).catch(console.error);
      this.generateAndSendTokenToDb();
      this.fetchTodos();
    });
  }
  fetchTodos = () => {
    this.db
      .collection("todos")
      .find({instanceId: this.state.instanceId})
      .asArray()
      .then((result) => {
        this.setState({todos : result})})
      .catch(console.error);
  };

  addTask = (task) => {
    return this.db.collection('todos').insertOne({...task,deadline: new Date(task.deadline), instanceId: this.state.instanceId});
  };

  toggleReminderForUser = (reminderStatus) => {
    const { instanceId ,fcmToken} = this.state;
    this.db
      .collection("instance_tokens")
      .findOneAndUpdate({ instanceId }, { instanceId,fcmToken, reminderStatus })
      .then(() =>this.setState({...this.state, reminderStatus}))
      .catch(console.error);
  }
}

export default App;
