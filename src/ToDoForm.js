import React, { Component } from "react";
import { Button, FormControl, InputGroup } from "react-bootstrap";
import Task from "./Task";
import DayPickerInput from "react-day-picker/DayPickerInput";
import moment from "moment";

class ToDoForm extends Component {
  state = {
    task: new Task({})
  };

  handleChange = event => {
    this.setState({
      task: { ...this.state.task, taskDescription: event.target.value }
    });
  };
  handleDayChange = day => {
    this.setState({
      task: { ...this.state.task, deadline: moment(day).format('MM/DD/YYYY') }
    });
  };

  render() {
    const { addToDo } = this.props;
    const { task } = this.state;
    return (
      <div>
        <InputGroup>
          <InputGroup.Prepend>
            <InputGroup.Text>Task Description</InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl id="todoInput" onChange={this.handleChange} />
        </InputGroup>
        <InputGroup>
          <InputGroup.Text>
            <img
              src={"https://image.flaticon.com/icons/svg/34/34389.svg"}
              height={20}
              width={35}
              alt={"text"}
            />
          </InputGroup.Text>
          <DayPickerInput
            value={this.state.task.deadline}
            onDayChange={this.handleDayChange}
            format={'M/D/YYYY'}
          />
        </InputGroup>
        <div>
          <Button
            className={"offset-4 col-3 marginTop10"}
            variant={"success"}
            size={"sm"}
            type={"button"}
            disabled={this.state.task.taskDescription === "" || this.state.task.deadline === ""}
            onClick={() => {
              addToDo(task);
            }}
          >
            Add Task
          </Button>
        </div>
      </div>
    );
  }
}

export default ToDoForm;
