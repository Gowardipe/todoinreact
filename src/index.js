import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-day-picker/lib/style.css';
import 'react-toggle-switch/dist/css/switch.min.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
ReactDOM.render(<App />, document.getElementById('root'));
    // if ('serviceWorker' in navigator) {
    //     let REACT_APP_PUBLIC_URL = '/todoinreact/';
    //     if(process.env.NODE_ENV === 'development') {
    //         console.log('in dev');
    //         REACT_APP_PUBLIC_URL = '../'
    //      }
    //     navigator.serviceWorker.register(`${REACT_APP_PUBLIC_URL}firebase-messaging-sw.js`)
    //     .then(function(registration) {
    //         console.log('Registration successful, scope is:', registration.scope);
    //     }).catch(function(err) {
    //     console.log('Service worker registration failed, error:', err);
    // });
// }
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
